import https from "https";
import fs from "fs";
import querystring from "querystring";
import Versions from "./upload.mjs";
import messages from "../utils/messages.mjs";
import log from "../utils/log.mjs";

const FS = fs.promises;

export default class Upload {
    constructor(args, cartridge, path) {
        const { username, password, hostname, active } = args;
        this.username  = username;
        this.password  = password;
        this.hostname  = hostname;
        this.cartridge = cartridge;
        this.active    = active;
        this.path      = path;
    }
    async post() {
        return new Promise(resolve => {
            const FILE = this.path.concat(this.cartridge.replace(/^/, "/") + ".zip");
            const HEADERS = this.options(this.cartridge);
            const REQUEST = https.request(HEADERS, ({ statusCode }) => resolve(statusCode));
            fs.createReadStream(FILE).pipe(REQUEST);
        });
    }
    options(file) {
        return {
            hostname: this.hostname,
            path: `/on/demandware.servlet/webdav/Sites/Cartridges/${this.active}/${file}/${file}.zip`,
            method: `PUT`,
            headers: {
                "Authorization": "Basic " + Buffer.from(this.username + ":" + this.password).toString("base64"),
                "User-Agent": "dwup",
                "Content-type": "application/zip",
                "Transfer-Encoding": "chunked"
            }
        }
    }
}
