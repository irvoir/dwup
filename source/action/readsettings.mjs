import path from "path";
import fs from "fs";
import log from "../utils/log.mjs";
import messages from "../utils/messages.mjs";
import login from "../auth/login.mjs";
import utility from "../utils/utility.mjs";

const FS = fs.promises;

const processDWJSON = object => {
    let keys = Object.keys(object);
    let values = Object.values(object);
    let isValid = str => str.length > 0 || Object.keys(str).length > 0 ? messages.OK : messages.NOT_OK;
    let status = [], isFirst;
    Array.from(keys).map((k, v) => {
        isFirst = v == 0 ? k[0].toUpperCase() + k.slice(1) : " " + k[0].toUpperCase() + k.slice(1);
        status.push(isFirst + ": " + isValid(Array.from(values)[v]));
    });
    log(status);
    return status.some(elem => elem.indexOf(messages.OK) > 0);
}

export default async function readSettings() {
    let SETTINGS, SETTINGS_OBJECT;
    try {
        SETTINGS = await FS.readFile("dw.json", "utf-8");
        SETTINGS_OBJECT = JSON.parse(SETTINGS);
        if (utility.isEmptyObject(SETTINGS_OBJECT)) {
            log(messages.CREDENTIAL_MISSING);
            process.exit(1);
        } else if (processDWJSON(SETTINGS_OBJECT)) {
            try {
                const CREDENTIALS = Object.values(SETTINGS_OBJECT).slice(0, 3);
                return await login(CREDENTIALS) ? SETTINGS_OBJECT : false;
            } catch (err) {
                return false;
            }
        }
    } catch (e) {
        log(e);
    }
}
