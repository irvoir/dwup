import https from "https";
import fs from "fs";
import querystring from "querystring";
import Versions from "./upload.mjs";
import messages from "../utils/messages.mjs";
import log from "../utils/log.mjs";

const FS = fs.promises;

export default class UploadFile {
    constructor(args, cartridge, path) {
        const { username, password, hostname, active } = args;
        this.username  = username;
        this.password  = password;
        this.hostname  = hostname;
        this.cartridge = cartridge;
        this.active    = active;
        this.path      = path;
    }
    async post() {
        return new Promise(resolve => {
            const FILE = this.path.concat(this.cartridge.replace(/^/, "/") + ".zip");
            const CARTINDEX = FILE.split('/').indexOf('cartridges') + 1;
            const REMOTEPATH = FILE.split('/').slice(CARTINDEX, FILE.split('/').length).join('/');
            const HEADERS = this.options(REMOTEPATH);
            const REQUEST = https.request(HEADERS, ({ statusCode }) => resolve(statusCode));
            fs.createReadStream(FILE).pipe(REQUEST);
        });
    }
    options(file) {
        return {
            hostname: this.hostname,
            path: `${messages.DEFAULT_CARTRIDGES_PATH}/${this.active}/${file}`,
            method: `PUT`,
            headers: {
                "Authorization": "Basic " + Buffer.from(this.username + ":" + this.password).toString("base64"),
                "User-Agent": "dwup",
                "Content-type": "application/zip",
                "Transfer-Encoding": "chunked"
            }
        }
    }
}
