import https from "https";
import messages from "../utils/messages.mjs";
import utility from "../utils/utility.mjs";

export default async function login(args) {
    let [ username, password, hostname ] = args;
    const httpOptions = {
        hostname: hostname,
        path: messages.DEFAULT_CARTRIDGES_PATH,
        method: "GET",
        agent: new https.Agent({ "User-Agent": messages.AGENT }),
        headers: {
            Authorization: utility.authHeader(username, password)
        }
    };
    return new Promise((resolve, reject) => {
        https.get(httpOptions, response => response.statusCode === 200 ? resolve(messages.OK) : reject(messages.NOT_OK));
    });
}
