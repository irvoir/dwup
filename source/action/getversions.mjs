import https from "https";
import messages from "../utils/messages.mjs";

export default class Versions {
    constructor({ username, password, hostname }) {
        this.transformBuffer = this.transformBuffer.bind(this);
        this.username = username;
        this.password = password;
        this.hostname = hostname;
    }
    async fetchCodeVersion() {
        return new Promise((resolve, reject) => {
            let VERSIONS;
            const REQUEST = https.get(this.options(), async (response) => {
                let { statusCode, statusMessage, headers } = response;
                if (statusCode > 200) {
                    for await (const value of response) {
                        VERSIONS = this.transformBuffer(value);
                    }
                    resolve({ message: messages.OK, data: VERSIONS });
                } else {
                    reject({ messages: messages.NOT_OK, data: "" });
                }
            });
            REQUEST.on("error", error => {
                reject({ messages: messages.NOT_OK, data: "" });
            });
        });
    }
    transformBuffer(buffer) {
        const LINES_NUMBER = buffer.toString("utf8").split(/\n/).length;
        const LINES_DATA = buffer.toString("utf8").split(/\n/);
        const VERSIONS = new Set();
        let match;
        for (let line of LINES_DATA) {
            if (line.search("displayname") > 0) {
                match = line.match(new RegExp(">" + "(.*)" + "<"))[1];
                match !== "" && !match.startsWith(".") ?
                    VERSIONS.add(match) : null;
            }
        }
        return VERSIONS;
    }
    options() {
        return {
            hostname: this.hostname,
            path: "/on/demandware.servlet/webdav/Sites/Cartridges/",
            method: "PROPFIND",
            agent: new https.Agent({ "User-Agent": "dwup" }),
            headers: {
                Authorization: "Basic " + Buffer.from(this.username+ ":" + this.password).toString("base64")
            }
        }
    }
}
