import https from "https";
import fs from "fs";
import Versions from "./source/action/getversions.mjs";
import LastFetch from "./source/action/writelastfetch.mjs";
import Build from "./source/action/build.mjs";
import Watch from "./source/action/watch.mjs";
import settings from "./source/action/readsettings.mjs";
import messages from "./source/utils/messages.mjs";
import utility from "./source/utils/utility.mjs";
import log from "./source/utils/log.mjs";
import path from "path";

const FS = fs.promises;
const now = new Date().toISOString();

void async function init() {
    try {
        /**
         *  Read dw.json file, if it's empty, then prompt user to complete it.
         */
        console.clear();
        let SETTINGS = await settings();
        if (!SETTINGS) {
            log(messages.INVALID_CREDENTIALS);
            log('Process will now exit');
        } else if (!utility.isEmptyObject(SETTINGS)) {
            /**
             * Decide if we fetch versions or use cache.
             */
            log(messages.LOGGED_IN + SETTINGS.username);
            if (SETTINGS.hasOwnProperty('last-fetch')) {
                switch (utility.timeElapsed(now, SETTINGS["last-fetch"].date)) {
                    case messages.NOT_OK:
                        utility.sliceObject(SETTINGS, 0, 3)
                            .then(data => utility.writeFile(data, messages.SETTINGS_NAME));
                        break;
                    case true:
                        log(utility.timeLeft(now, SETTINGS["last-fetch"].date) + messages.TIME_NOT_PASSED);
                        log(messages.ENTER_WATCH);
                        new Watch(SETTINGS);
                        break;
                    case false:
                        log(messages.TIME_PASSED);
                        let newVersions = await new Versions(SETTINGS).fetchCodeVersion();
                        let newFetch = await new LastFetch(SETTINGS, newVersions.data);
                        await new Build(SETTINGS);
                        break;
                }
            } else {
                log(messages.FIRST_FETCH);
                try {
                    let newVersions = await new Versions(SETTINGS).fetchCodeVersion();
                    switch (newVersions.message) {
                        case messages.OK:
                            await new LastFetch(SETTINGS, newVersions.data);
                            await new Build(SETTINGS);
                            break;
                        case messages.NOT_OK:
                            log(messages.NOT_OK);
                            process.exit(1);
                            break;
                    }
                } catch (err) {
                    log(err);
                }
            }
        } else {
            log(messages.SETTINGS_MISSING);
        }
    } catch (e) {
        log(messages.ERROR_APP);
    }
}();
