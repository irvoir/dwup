import fs from "fs";
import path from "path";
import util from "util";
import childProcess from "child_process";
import log from "../utils/log.mjs";
import messages from "../utils/messages.mjs";
import utility from "../utils/utility.mjs";
import Mkdir from "./mkdir.mjs";
import Upload from "./upload.mjs";
import Unzip from "./unzip.mjs";
import Delete from "./delete.mjs";
import Watch from "./watch.mjs";

const FS = fs.promises;

export default class Build {
    constructor(args, cartridge) {
        this.args = args;
        this.uploadCartridge = this.uploadCartridge.bind(this);
        this.uploadCartridges = this.uploadCartridges.bind(this);
        this.cartridge = cartridge || false;
        cartridge ? this.uploadCartridge.call(this) : this.uploadCartridges.call(this);
    }
    async progress(progress) {
        process.stdout.write(progress);
    }
    async sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async uploadCartridge() {
        try {
            const updateWatch = await FS.readFile(".watch.json", { encoding: "utf8" });
            if (util.isArray(this.cartridge) && this.cartridge.length > 1) {
                for await (let value of this.cartridge) {
                    const file = value.substring(value.lastIndexOf("/") + 1, value.length);
                    const cartridge = utility.getCartridge(value);
                    const path = utility.getCartridgePath(value);
                    const exec = util.promisify(childProcess.exec);
                    await exec(`zip -r ${cartridge}.zip *`, { cwd: path, maxBuffer: 500 * 1024 });
                    await new Mkdir(this.args, cartridge, path).mkdir()
                        .then(await new Upload(this.args, cartridge, path).post())
                        .then(await new Unzip(this.args, cartridge).unzip())
                        .then(await new Delete(this.args, cartridge).delete())
                        .then(this.progress(messages.FINISH_CARTRIDGE + cartridge + messages.END_MSG))
                        .catch(() => log(messages.EXIST_CARTRIDGES));
                    await exec(`rm ${cartridge}.zip`, { cwd: path });
                }
                if (Object.values(JSON.parse(updateWatch)).length > 0) {
                    await new Watch(this.args, true).watch(JSON.parse(updateWatch));
                } else await new Watch(this.args);
            } else {
                const toUploadPath = this.cartridge.toString();
                const cartridge = utility.getCartridge(toUploadPath);
                process.stdout.write(messages.DELAY_CARTRIDGE + cartridge + messages.END_MSG);
                await this.sleep(10000);
                const file = toUploadPath.substring(toUploadPath.lastIndexOf("/") + 1, toUploadPath.length);
                const path = utility.getCartridgePath(toUploadPath);
                const exec = util.promisify(childProcess.exec);
                await exec(`zip -r ${cartridge}.zip *`, { cwd: path, maxBuffer: 500 * 1024 });
                await new Mkdir(this.args, cartridge, path).mkdir()
                    .then(await new Upload(this.args, cartridge, path).post())
                    .then(await new Unzip(this.args, cartridge).unzip())
                    .then(await new Delete(this.args, cartridge).delete())
                    .then(this.progress(messages.FINISH_CARTRIDGE + cartridge + messages.END_MSG))
                    .catch(() => log(messages.EXIST_CARTRIDGES));
                await exec(`rm ${cartridge}.zip`, { cwd: path });
                if (Object.values(JSON.parse(updateWatch)).length > 0) {
                    await new Watch(this.args, true).watch(JSON.parse(updateWatch));
                } else await new Watch(this.args);
            }
        } catch (err) {
            log(err);
            return false;
        }
    }
    async uploadCartridges() {
        try {
            let count = 0;
            const toWrite = {};
            const exec = util.promisify(childProcess.exec);
            const cartridges = await this.getCartridgesNames();
            const length = cartridges.length;
            if (length > 0) {
                for await (let value of await this.getCartridgesPaths()) {
                    count++;
                    const fileArr = value.split("/");
                    const cartridge = fileArr[fileArr.length - 1];
                    await exec(`zip -r ${cartridge}.zip *`, { cwd: value, maxBuffer: 500 * 1024 });
                    await new Mkdir(this.args, cartridge, value).mkdir()
                        .then(await new Upload(this.args, cartridge, value).post())
                        .then(await new Unzip(this.args, cartridge).unzip())
                        .then(await new Delete(this.args, cartridge).delete())
                        .then(this.progress(messages.FINISH_CARTRIDGE + cartridge + ` => ${count}/${length}` + messages.END_MSG))
                        .catch(() => log(messages.EXIST_CARTRIDGES));
                    toWrite[count] = value;
                    await exec(`rm ${cartridge}.zip`, { cwd: value });
                }
                log(messages.FINISH_UPLOAD);
                await utility.writeFile(toWrite, ".build.json");
                await new Watch(this.args);
            } else {
                log(messages.NO_CARTRIDGES);
            }
        } catch (err) {
            log(err);
        }
    }
    async readFilesData(files) {
        try {
            return [].concat(...files)
                .map(async e => await FS.stat(e)
                    .then(data => data.mtime.toISOString())
                    .catch(err => log(err.path))
                )
                .filter(e => e);
        } catch (err) {
            log(err);
            process.exit(1);
        }
    }
    async getCartridgesNames() {
        let files = await this.getFiles(messages.ROOT);
        return [...new Set(files
            .filter(e => e.includes(messages.CARTRIDGES))
            .map(e => utility.getCartridge(e))
            .filter(e => e)
        )];
    }
    async getCartridgesPaths() {
        let files = await this.getFiles(messages.ROOT);
        return [...new Set(files
            .filter(e => e.includes(messages.CARTRIDGES))
            .map(e => utility.getCartridgePath(e))
            .filter(e => e)
        )];
    }
    async sleep (ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async getFiles(dir) {
        try {
            const subdirs = await FS.readdir(dir);
            const files = await Promise.all(subdirs
                .map(async subdir => {
                    const res = path.resolve(dir, subdir);
                    if (res.includes("/.") || res.includes("/node_modules") || res.includes("/System") || res.includes("/$REC")) return;
                    await this.sleep(2000);
                    return (await FS.stat(res)).isDirectory() ? this.getFiles(res) : res
                })
            );
            return files
                .filter(e => e)
                .reduce((a, f) => a.concat(f), []);
        } catch (err) {
            if ("path" in err) {
                return (await FS.stat(err.path)).isDirectory() ? this.getFiles(err.path) : err.path;
            } else {
                log("Panic!");
            }
        }
    }
}
