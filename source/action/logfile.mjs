const cartridges = await this.getCartridgesNames();
if (cartridges.length > 0) {
    const paths = await this.getCartridgesPaths();
    let files, times, writeObject = {}, allFiles = [], allTimes = [];
    for await (let value of paths) {
        files = utility.flattenArray(await this.getFiles(value));
        allFiles.push(files);
        times = await Promise.all(await this.readFilesData(files));
        allTimes.push(times);
        await this.sleep(100);
    }
    for (const value of Object.keys(utility.flattenArray(allFiles))) {
        writeObject[value] = {
            cartridge: utility.getCartridge(utility.flattenArray(allFiles)[value]),
            file: utility.flattenArray(allFiles)[value],
            time: utility.flattenArray(allTimes)[value]
        }
    }
    if (await utility.writeFile(writeObject, ".temp.json") == messages.OK) {
        const exec = util.promisify(child_process.exec);
        const notUploaded = new Map();
        for await (let value of paths) {
            const fileArr = value.split('/');
            const cartridge = fileArr[fileArr.length - 1];
            await exec(`zip -r ${cartridge}.zip *`, { cwd: value });
            await new Mkdir(this.args, cartridge, value).mkdir()
                .then(await new Upload(this.args, cartridge, value).post())
                .then(await new Unzip(this.args, cartridge).unzip())
                .then(await new Delete(this.args, cartridge).delete())
                .catch(err => log(err));
        }
    }
} else {
    log('Couldn\'t find any cartridges');
}
