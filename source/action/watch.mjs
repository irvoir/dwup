import fs from "fs";
import path from "path";
import util from "util";
import child from "child_process";
import log from "../utils/log.mjs";
import messages from "../utils/messages.mjs";
import settings from "../utils/settings.mjs";
import utility from "../utils/utility.mjs";
import UploadFile from "../action/uploadfile.mjs";
import UnzipFile from "../action/unzipfile.mjs";
import DeleteFile from "../action/deletefile.mjs";
import Build from "./build.mjs";

const FS = fs.promises;

export default class Watch {
    constructor(args, ignore) {
        this.args = args;
        this.sleep = this.sleep.bind(this);
        this.watch = this.watch.bind(this);
        this.write = this.write.bind(this);
        this.parseFileName = this.parseFileName.bind(this);
        if (!ignore) this.init();
    }
    async init() {
        await this.emptyFiles();
    }
    async emptyFiles() {
        try {
            const encoding = { encoding: "utf8" };
            const BUILD = await FS.readFile(".build.json", encoding);
            const BUILD_OBJECT = JSON.parse(BUILD);
            await utility.writeFile({}, ".times.json");
            await utility.writeFile({}, ".watch.json");
            if (Object.keys(BUILD_OBJECT).length) {
                await this.write();
            } else {
                console.clear();
                process.stdout.write(messages.NEW_BUILD);
                await new Build(this.args);
            }
        } catch(err) {
            if (err.code === "ENOENT") {
                await utility.writeFile({}, err.path);
                this.emptyFiles();
            }
        }
    }
    async write() {
        try {
            const JSONREAD = JSON.parse(await this.build());
            const exec     = util.promisify(child.exec);
            const PATHS    = new Set();
            const RAW      = new Set();
            const STORE    = {};
            let count = 0;
            for (let [key, val] of Object.entries(JSONREAD)) {
                if((path.join(val + "../../")).includes("cartridges"))
                    PATHS.add(path.join(val + "../../../"))
            }
            for await (let val of PATHS) {
                let { stdout: hashdiff } = await exec("git diff-files", { cwd: val });
                hashdiff
                    .split(":")
                    .filter(e => e)
                    .map(e => RAW.add(e));
            }
            for (let value of RAW) {
                let line = value.split("/").map(e => e.includes("cartridges") ? "cartridges" : e);
                STORE[count++] = line.join("/").trim();
            }
            await utility.writeFile(STORE, ".watch.json");
            if (Object.keys(STORE).length > 0) {
                this.watch(STORE);
            } else {
                let times = await FS.readFile(".times.json", { encoding: "utf8" });
                let timesObject = Object.values(JSON.parse(times));
                if (timesObject.length) {
                    console.clear();
                    process.stdout.write(messages.NEW_BUILD);
                    await new Build(this.args, [].concat(...timesObject));
                } else {
                    process.stdout.write(messages.NO_CHANGES + messages.WAIT_TIME_INTERVAL + messages.END_MSG);
                    await this.sleep(10000);
                    this.write();
                }
            }
        } catch (err) {
            if (err.code === "ENOENT") {
                console.clear();
                process.stdout.write(messages.NEW_BUILD);
                await new Build(this.args);
            }
        }
    }
    parseFileName(filename) {
        const toCheck = path.basename(filename);
        return (toCheck.charAt(0) !== ".") ? filename : false;
    }
    secondsParse(d) {
        d = Number(d);
        const h = Math.floor(d / 3600);
        const m = Math.floor(d % 3600 / 60);
        const s = Math.floor(d % 3600 % 60);
        const hour = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
        const minute = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
        const second = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
        return hour + minute + second;
    }
    async printLatest(file, sec) {
        const str = ` ] :: [ Latest file: ${file} ] :: [ Uploaded: ${this.secondsParse(sec)} ago ] ::`;
        process.stdout.write(messages.FILE_NOT_CHANGED + str);
    }
    async watch(obj) {
        try {
            const exec = util.promisify(child.exec);
            const JSONREAD = JSON.parse(await this.build());
            const CARTRIDGES = Object.values(obj)
                .filter(val => val.startsWith("cartridges"))
                .reduce((acc, val) => {
                    let cartridge = val.split("/")[1];
                    let find = Object.values(JSONREAD)
                        .filter(val => (val.substring(val.lastIndexOf("/") + 1, val.length) === cartridge));
                    let path = find.toString();
                    let paths = [...new Set(path.split("/").concat(val.split("/")))].join("/");
                    this.parseFileName(paths) ? acc.push(paths) : null;
                    acc.push(paths);
                    return acc;
                }, []);
            const writeFile = {};
            for await (let value of CARTRIDGES) {
                let ctime = new Date((await this.readStats(value)).ctime).getTime();
                writeFile[ctime] = value;
            }
            let readFileRAW = await FS.readFile(".times.json", { encoding: "utf8" });
            let latestFileJSON  = JSON.parse(await FS.readFile(".temp.json", { encoding: "utf8" }));
            let readFile = JSON.parse(readFileRAW);
            if (readFileRAW) {
                let latestFile, latestPath, pathNoFile;
                if (await utility.equalObjects(writeFile, readFile)) {
                    let time = Object.keys(latestFileJSON);
                    let file = Object.values(latestFileJSON);
                    let pass = (Date.now() - time) / 1000;
                    await this.printLatest(file, pass);
                    await this.sleep(2500);
                    this.write();
                } else {
                    const ARR_NEW = Object.keys(writeFile);
                    const ARR_OLD = Object.keys(readFile);
                    let UPDATE_BUILD = false;
                    if (ARR_NEW.length > ARR_OLD.length) {
                        let difference = ARR_NEW.filter(x => !ARR_OLD.includes(x));
                        for await (let value of difference) {
                            latestPath = writeFile[value];
                            latestFile = await this.getFileName(writeFile[value]);
                            pathNoFile = writeFile[value].replace(`/${latestFile}`, "");
                            await FS.writeFile(".temp.json", JSON.stringify({ [Date.now()]: latestFile }, null, 4));
                            if (path.parse(latestFile).ext === ".scss" || path.parse(latestFile).ext === '.css') {
                                UPDATE_BUILD = latestPath;
                            }
                            await exec(`zip -r ${latestFile}.zip ${latestFile}`, { cwd: pathNoFile });
                            const status = await new UploadFile(this.args, latestFile, pathNoFile).post();
                            if (status === 201) {
                                await new UnzipFile(this.args, latestPath).unzip();
                                await new DeleteFile(this.args, latestPath).delete();
                                process.stdout.write(messages.FILE_CHANGED + `] :: [ Uploaded: ${latestFile} ] ::`);
                                await this.sleep(1500);
                            } else {
                                await new UnzipFile(this.args, latestPath).unzip();
                                await new DeleteFile(this.args, latestPath).delete();
                                process.stdout.write(`:: [ Couldn't upload ${latestFile}, code: ${status} ] ::`);
                                await this.sleep(1500);
                            }
                            await exec(`rm ${latestFile}.zip`, { cwd: pathNoFile });
                        }
                    } else if(ARR_NEW.length === ARR_OLD.length) {
                        let difference = await utility.getDifference(writeFile, readFile);
                        for await (let value of Object.values(difference)) {
                            latestPath = value;
                            latestFile = await this.getFileName(value);
                            pathNoFile = value.replace(`/${latestFile}`, "");
                            await FS.writeFile(".temp.json", JSON.stringify({ [Date.now()]: latestFile }, null, 4));
                            if (path.parse(latestFile).ext === ".scss" || path.parse(latestFile).ext === '.css') {
                                UPDATE_BUILD = latestPath;
                            }
                            await exec(`zip -r ${latestFile}.zip ${latestFile}`, { cwd: pathNoFile });
                            const status = await new UploadFile(this.args, latestFile, pathNoFile).post();
                            if (status === 201) {
                                await new UnzipFile(this.args, latestPath).unzip();
                                await new DeleteFile(this.args, latestPath).delete();
                                process.stdout.write(messages.FILE_CHANGED + "uploaded => " + latestFile + messages.END_MSG);
                                await this.sleep(1500);
                            } else {
                                await new UnzipFile(this.args, latestPath).unzip();
                                await new DeleteFile(this.args, latestPath).delete();
                                process.stdout.write(`:: [ Couldn't upload ${latestFile}, code: ${status} ] ::`);
                                await this.sleep(1500);
                            }
                            await exec(`rm ${latestFile}.zip`, { cwd: pathNoFile });
                        }
                    }
                    await FS.writeFile(".times.json", JSON.stringify(writeFile, null, 4));
                    UPDATE_BUILD ? await new Build(this.args, UPDATE_BUILD) : this.write();
                }
            } else {
                await FS.writeFile(".times.json", JSON.stringify(writeFile, null, 4));
            }
        } catch (err) {
            if (err.path) {
                console.clear();
                log(`Some paths couldn\'t be read: ${err.path}, a new build will start`);
                if (err.path.charAt(0) == '.') {
                    await utility.writeFile({}, err.path);
                } else {
                    process.stdout.write(messages.NEW_BUILD);
                }
                await new Build(this.args);
            } else if (err.path !== undefined) {
                utility.writeFile({}, err.path);
            }
        }
    }
    async readStats(file) {
        return FS.stat(file);
    }
    async rmFile(file) {
        return FS.unlink(file);
    }
    async getFileName(str) {
        await this.sleep(1000);
        return path.basename(str);
    }
    async build() {
        return await FS.readFile(".build.json", "utf-8");
    }
    async sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
