import utility from "../utils/utility.mjs";
import messages from "../utils/messages.mjs";

export default class LastFetch {
    constructor(SETTINGS, data) {
        this.username =  SETTINGS.username;
        this.password =  SETTINGS.password;
        this.hostname =  SETTINGS.hostname;
        this.active   =  SETTINGS.active || false;
        this.data = data;
        this.write();
    }
    write() {
        let now = new Date().toISOString();
        let data = {
            username: this.username,
            password: this.password,
            hostname: this.hostname,
            ...(this.active ? {active: this.active} : ''),
            "last-fetch": {
                date: now,
                versions: [...this.data]
            }
        }
        utility.writeFile(data, messages.SETTINGS_NAME);
    }
}
