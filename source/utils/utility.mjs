import fs from "fs";
import assert from "assert";
import log from "./log.mjs";
import messages from "./messages.mjs";
const FS = fs.promises;

export default class Utility {
    constructor() {}
    static isEmptyObject(object) {
        return Object.keys(object).length === 0 && object.constructor === Object;
    }
    static authHeader(username, password) {
        return "Basic " + Buffer.from(username + ":" + password).toString("base64");
    }
    static isDate(date) {
        return Date.parse(date);
    }
    static timeElapsed(ftime, stime) {
        return Utility.isDate(ftime) && Utility.isDate(stime) ?
            (Math.abs(Utility.isDate(ftime) - Utility.isDate(stime)) / 3600000 < 24) :
            messages.NOT_OK;
    }
    static timeLeft(ftime, stime) {
        return (24 - Math.abs(Utility.isDate(ftime) - Utility.isDate(stime)) / 3600000).toFixed(2) + ' hours ';
    }
    static async writeFile(data, filename) {
        try {
            Utility.isEmptyObject(data) ?
                await FS.writeFile(filename, '{}') :
                await FS.writeFile(filename, JSON.stringify(data, null, 4));
        } catch (err) {
            if (err.path !== 'undefined') {
                Utility.writeFile({}, err.path);
            }
            return messages.NOT_OK;
        }
        return new Promise(resolve => resolve(messages.OK));
    }
    static async sliceObject(object, start, end) {
        return Object.keys(object)
            .slice(start, end)
            .reduce((acc, key) => {
                acc[key] = object[key];
                return acc;
            }, {});
    }
    static getCartridge(path) {
        let isPath = path.substring(path.indexOf(messages.CARTRIDGES), path.length);
        let indexes = path.split('/');
        if ((isPath.split('/').length - 1) >= 2) {
            let carts = indexes.indexOf(messages.CARTRIDGES);
            if (!indexes[carts + 1].includes('.') && indexes[carts + 2] == messages.CARTRIDGES.slice(0, -1)) {
                return indexes[carts + 1];
            }
        }
    }
    static getCartridgePath(path) {
        let isPath = path.substring(path.indexOf(messages.CARTRIDGES), path.length);
        let indexes = path.split('/');
        if ((isPath.split('/').length - 1) >= 2) {
            let carts = indexes.indexOf(messages.CARTRIDGES);
            if (!indexes[carts + 1].includes('.') && indexes[carts + 2] == messages.CARTRIDGES.slice(0, -1)) {
                return indexes.slice(0, indexes.indexOf(indexes[carts + 2])).join('/');
            } else if (path.includes('modules')) {
                return indexes.slice(0, indexes.indexOf(indexes[carts + 2])).join('/');
            }
        }
    }
    static flattenArray(array) {
        return [].concat(...array);
    }
    static async equalObjects(obj1, obj2) {
        try {
            assert.deepEqual(obj1, obj2);
        } catch (err) {
            if (err.code === 'ERR_ASSERTION') {
                return false;
            }
            throw err;
        }
        return true;
    }
    static async getDifference(obj1, obj2) {
        return Object.keys(obj2).reduce((diff, key) => {
            if (obj1[key] === obj2[key]) return diff
            return {
                ...diff,
                [key]: obj2[key]
            }
        }, {})
    }
}
