export default function replanim(string) {
    var P = ["\\", "|", "/", "-"];
    var x = 0, y;
    return setInterval(function() {
        process.stdout.write(P[x++] + "\r" + `:: [ ${string} ] :: `);
        x &= 3;
    }, 150);
};
