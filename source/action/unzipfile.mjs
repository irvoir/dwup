import https from "https";
import querystring from "querystring";
import messages from "../utils/messages.mjs";
import log from "../utils/log.mjs";

export default class UnzipFile {
    constructor(args, file) {
        const { username, password, hostname, active } = args;
        this.username  = username;
        this.password  = password;
        this.hostname  = hostname;
        this.active    = active;
        this.file      = file;
    }
    async unzip() {
        return new Promise(resolve => {
            const POST_DATA = querystring.stringify({ method: "UNZIP" });
            const CARTINDEX = this.file.split('/').indexOf('cartridges') + 1;
            const REMOTEPATH = this.file.split('/').slice(CARTINDEX, this.file.split('/').length).join('/');
            const HEADERS = this.options(REMOTEPATH);
            const REQUEST = https.request(HEADERS, ({ statusCode }) => resolve(statusCode));
            REQUEST.write(POST_DATA);
            REQUEST.end();
        });
    }
    options(file) {
        return {
            hostname: this.hostname,
            path: `${messages.DEFAULT_CARTRIDGES_PATH}/${this.active}/${file}.zip`,
            method: "POST",
            headers: {
                "Authorization": "Basic " + Buffer.from(this.username + ":" + this.password).toString("base64"),
                "User-Agent": "dwup",
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }
    }
}
