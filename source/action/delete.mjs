import https from "https";
import fs from "fs";
import querystring from "querystring";
import Versions from "./upload.mjs";
import messages from "../utils/messages.mjs";
import log from "../utils/log.mjs";

const FS = fs.promises;

export default class Delete {
    constructor(args, cartridge) {
        const { username, password, hostname, active } = args;
        this.username  = username;
        this.password  = password;
        this.hostname  = hostname;
        this.active    = active;
        this.cartridge = cartridge;
    }
    async delete() {
        return new Promise(resolve => {
            const HEADERS = this.options(this.cartridge);
            const REQUEST = https.request(HEADERS, ({ statusCode }) => resolve(statusCode));
            REQUEST.end();
        });
    }
    options(path) {
        return {
            hostname: this.hostname,
            path: `/on/demandware.servlet/webdav/Sites/Cartridges/${this.active}/${path}/${path}.zip`,
            method: `DELETE`,
            headers: {
                "Authorization": "Basic " + Buffer.from(this.username + ":" + this.password).toString("base64"),
                "User-Agent": "dwup"
            }
        }
    }
}
